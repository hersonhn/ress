module.exports = {
  updateInterval: 10 * 60 * 1000, // ten minutes
  port: process.env.RESS_PORT || 4100
}
